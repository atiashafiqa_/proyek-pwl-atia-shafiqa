<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('index');
});

Route::get('/about', function () {
    $nama = 'Atia Shafiqa';
    return view('about', ['nama' => $nama]);
});

Route::get('/experiences', function () {
    // get data from a database
    $experiences = [
      ['type' => 'Project Manager at Google', 'base' => '2025'],
      ['type' => 'UX Researcher at Google', 'base' => '2024'],
      ['type' => 'Computer Science Lecturer at NTU Singapore', 'base' => '2023'],
      ['type' => 'UI/UX Designer Lead at Shopee Singapore', 'base' => '2023'],
      ['type' => 'UI/UX Designer at Shopee Singapore', 'base' => '2022'],
      ['type' => 'Software Enggineer at Gojek', 'base' => '2022'],
      ['type' => 'Junior Front-end Developer at Artha.co', 'base' => '2021']
    ];
    return view('experiences', ['experiences' => $experiences]);

  });